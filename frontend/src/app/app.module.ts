import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomTooltip } from './custom-tooltip.component';


import { AgGridModule} from 'ag-grid-angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataInterceptor } from './data.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    CustomTooltip
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AgGridModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [{provide:HTTP_INTERCEPTORS,useClass: DataInterceptor,multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
