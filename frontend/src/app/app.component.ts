import { Component, OnInit, ViewChild } from '@angular/core';
import { CellValueChangedEvent,
  ColDef,
  CellClassRules,
  ValueFormatterParams,
  RowSelectedEvent, 
  CellEditingStartedEvent,
  CellEditingStoppedEvent,
  GridReadyEvent} from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import { ApiService } from './api.service';
import { CustomTooltip } from './custom-tooltip.component';
import { TableData } from './interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
}) export class AppComponent implements OnInit {

  title: string = 'GreenIT Application Challenge';

  errorMessage:string = '';

  cantSaveError: string = '';

  isSaveAttempted:boolean = false;

  rowData: TableData[] = [];

  tooltipShowDelay:number = 0;

  tooltipHideDelay:number = 2000;

  dataToSave: any[] = [];

  validationPass:boolean = true;

  cellValueChanged:boolean = false;

  cellEditStopped:boolean = false;

  showSaved: boolean = false;

  notSaved: boolean = false;

  deleted: boolean = false;

  selectedCount:number = 0;

  stringRules: CellClassRules = {
    'rag-red': (params) => this.isSaveAttempted && params.value == ''
  };

  stateRules: CellClassRules = {
    'rag-red': (params) => this.isSaveAttempted && (params.value == '' || params.value.length != 2 || !isNaN(params.value))
  };

  zipRules: CellClassRules = {
    'rag-red': (params) => this.isSaveAttempted && (params.value == '' || params.value.length != 5 || isNaN(params.value)) 
  };

  numberRules: CellClassRules = {
    'rag-red': (params) => this.isSaveAttempted && (params.value == '' || isNaN(params.value)) 
  }

  defaultColDef: ColDef = {
    sortable: true, filter: true, tooltipComponent: CustomTooltip
  }

  columnDefs: ColDef[] = [
    { headerName: 'ID',
      field: 'id',
      headerCheckboxSelection: true,
      checkboxSelection: true,
      showDisabledCheckboxes: true },
    { headerName: 'Name',
      field: 'name',
      editable: true,
      valueFormatter: this.ucFAplhabetFormatter,
      tooltipField: 'name',
      tooltipComponentParams: { color: '#ececec' },
      cellClassRules: this.stringRules},
    { headerName: 'State', 
      field: 'state',
      editable: true,
      valueFormatter: this.ucAlphabetFormatter,
      cellClassRules: this.stateRules},
    { headerName: 'Zip',
      field: 'zip',
      editable: true,
      valueFormatter: this.numberFormatter,
      cellClassRules: this.zipRules},
    { headerName: 'Amount',
      field: 'amount',
      editable: true,
      valueFormatter: this.currencyFormatter,
      cellClassRules: this.numberRules},
    { headerName: 'Quantity',
      field: 'qty',
      editable: true,
      valueFormatter: this.numberFormatter,
      cellClassRules: this.numberRules},
    { headerName: 'Item',
      field: 'item',
      editable: true,
      valueFormatter: this.ucFormatter,
      cellClassRules: this.stringRules},
  ]

  autoGroupColumnDef = {
    headerName: 'ID',
    field: 'id',
    cellRenderer: 'agGroupCellRenderer',
    cellRendererParams: { checkbox: true },
  };
  
  @ViewChild(AgGridAngular) agGrid!: AgGridAngular;

  constructor(private apiData: ApiService){}

  ngOnInit(){}

  fetchFromServer(){
    this.apiData.readData().subscribe((res:TableData[]) => {
      this.rowData = Array.from(Object.values(res));
    }, err => {
      this.rowData = [];
      this.errorMessage = err;
    });
  }

  onGridReady(params: GridReadyEvent<TableData>) {
    this.fetchFromServer();
    this.agGrid.api.sizeColumnsToFit();
  }

  onRowSelected(event: RowSelectedEvent){
    var selectedData = this.agGrid.api.getSelectedRows ();
    this.selectedCount = selectedData.length;
  }

  onCellValueChanged(event: CellValueChangedEvent){
    this.cellValueChanged = true;
  }

  onCellEditingStarted(event: CellEditingStartedEvent){
    this.cellEditStopped = false;
  }

  onCellEditingStopped(event: CellEditingStoppedEvent){
    this.cellEditStopped = true;
  }

  getAllRecords(){
    this.validationPass = true;
    this.dataToSave = [];
    this.agGrid.api.forEachNode((rowNode) => {
      rowNode.setDataValue('amount', parseFloat(rowNode.data['amount']).toFixed(2));
      
      let name:string = rowNode.data['name'];
      let state:string = rowNode.data['state'];
      let zip:number = rowNode.data['zip'];
      let amount:number = rowNode.data['amount'];
      let qty:number = rowNode.data['qty'];
      let item:string = rowNode.data['item'];
      rowNode.setDataValue('state', state.toUpperCase());
      rowNode.setDataValue('item', item.toUpperCase());
      if ('' == name.replace(/\s/g, "")){
        this.agGrid.api.flashCells({ rowNodes: [rowNode], columns: ['name'] });
        this.validationPass = false;
      }else if('' == item.replace(/\s/g, "")){
        this.agGrid.api.flashCells({ rowNodes: [rowNode], columns: ['item'] });
        this.validationPass = false;
      }else if(isNaN(amount) || '' == amount.toString().replace(/\s/g, "")){
        this.agGrid.api.flashCells({ rowNodes: [rowNode], columns: ['amount'] });
        this.validationPass = false;
      }else if('' == qty.toString().replace(/\s/g, "") || isNaN(qty)){
        this.agGrid.api.flashCells({ rowNodes: [rowNode], columns: ['qty'] });
        this.validationPass = false;
      }else if(zip.toString().length != 5 ||
      isNaN(zip) ||
      '' == zip.toString().replace(/\s/g, "")){
        this.agGrid.api.flashCells({ rowNodes: [rowNode], columns: ['zip'] });
        this.validationPass = false;
      }else if('' == state.replace(/\s/g, "") || /\d/.test(state) ||
      state.length != 2){
        this.agGrid.api.flashCells({ rowNodes: [rowNode], columns: ['state'] });
        this.validationPass = false;
      }
      this.dataToSave.push(rowNode.data);
    });
  }

  onAddRow(){
    this.getAllRecords();
    if(this.dataToSave.length > 0){
      var lastId = this.dataToSave[this.dataToSave.length - 1]['id'];
      var id = parseInt(lastId)+1;
    }else {
      var id = 1;
    }
    this.agGrid.api.applyTransaction({add: [{id: id, name: '', state: '', zip: '', amount: '', qty: '', item: '', newlyAdded: true}]});
  }

  updateData(){
    this.isSaveAttempted = true;
    this.getAllRecords();
    if(this.validationPass){
      this.apiData.updateData(this.dataToSave).subscribe(res => {
        this.notSaved = false;
        this.showSaved = true;
        this.cellEditStopped = false;
        this.isSaveAttempted = false;
        this.fetchFromServer();
        setTimeout(()=>{
          this.showSaved = false;
        }, 3000);
      }, err => {
        this.notSaved = true;
        this.cantSaveError = "Can't Save the Data! "+err;
      }); 
    }else {
      this.agGrid.api.redrawRows();
    }
  }

  onDeleteRow(){
    if(confirm("Are you sure to delete")) {
      var selectedData = this.agGrid.api.getSelectedRows ();
      selectedData.forEach(element => {
        if(element.newlyAdded){
          this.agGrid.api.applyTransaction({remove: selectedData});
        }else{
          this.apiData.deleteData(element.id).subscribe(res => {
            this.deleted = true;
            this.agGrid.api.applyTransaction({remove: selectedData});
          }, err => {
            this.deleted = false;
            this.notSaved = true;
            this.cantSaveError = "Can't delete! "+err;
            //Data not deleted in Server.
          });
        }
      });
    }
  }

  clearSelection(){
    this.agGrid.api.deselectAll();
    this.selectedCount = 0;
  }

  currencyFormatter(params: ValueFormatterParams){
    return params.value != '' ? parseFloat(params.value).toFixed(2).toString() : '';
  }

  numberFormatter(params: ValueFormatterParams){
    return params.value != '' ? parseInt(params.value).toString() : '';
  }

  ucAlphabetFormatter(params: ValueFormatterParams){
    let str = params.value;
    return str.toUpperCase().replace(/[^a-zA-Z ]/g, '');
  }

  ucFAplhabetFormatter(params: ValueFormatterParams){
    let str = params.value;
    return str.replace(/[^a-zA-Z ]/g, '');
  }

  ucFormatter(params: ValueFormatterParams){
    let str = params.value;
    return str.toUpperCase();
  }
}
