import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TableData } from './interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  PHP_API_SERVER = "http://localhost/application-test/backend";

  constructor(private httpClient: HttpClient) {

  }

  readData(): Observable<TableData[]>{
    return this.httpClient.get<TableData[]>(`${this.PHP_API_SERVER}/api/read.php`);
  }

  updateData(data: any){
    let jsonData = JSON.stringify(data);
    return this.httpClient.put<TableData>(`${this.PHP_API_SERVER}/api/update.php`, jsonData, {responseType: 'json'});   
  }

  deleteData(id: number){
    return this.httpClient.delete<TableData>(`${this.PHP_API_SERVER}/api/delete.php/?id=${id}`);
  }

}
