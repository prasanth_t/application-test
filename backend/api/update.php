<?php
/**
 * Set headers to access the origin, methods and content type from Frontend.
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");
header("Content-Type:application/json");

/**
 * include the Class file to access their members.
 */
include_once "class-data.php";

// Get the posted data and Decode it.
$postdata = json_decode(file_get_contents("php://input"), true);

/**
 * Pass the data to be updated which is returned by Frontend Framework.
 */
$data->update($postdata);