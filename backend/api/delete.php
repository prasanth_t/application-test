<?php
/**
 * Set headers to access the origin and methods from Frontend.
 */
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");

/**
 * include the Class file to access their members.
 */
include_once "class-data.php";

// Extract, validate and sanitize the id.
$id = (isset($_GET['id']) && $_GET['id'] !== null && (int)$_GET['id'] > 0)? (int)$_GET['id'] : false;

/**
 * Return 400 response if Id is undefined.
 */
if(!$id){
    return http_response_code(400);
}

/**
 * Pass the Id which is unlink from the PHP array.
 */
$data->delete($id);
