<?php
/**
 * Data Class
 *
 * @package    Sample Data
 */

/**
 * Data Class
 */
class Data {

	/**
	 * Array which stores data from CSV.
	 */
	private $result = array();

	/**
	 * CSV file name.
	 */
	private $file_name = 'data.csv';

	/**
	 * Define the core functionality of the Class.
	 */
	public function __construct(){
		$this->read_csv();
	}

	/**
	 * delete the value of array by Id.
	 */
	public function delete($id) {
		if(is_array($this->result) && !empty($this->result)){
			foreach($this->result as $key => $value){
				if($id == $value['id']){
					/**
					 * Unset the entry from an Array.
					 */
					unset($this->result[$key]);
				}
			}
			/**
			 * Update the array in to CSV.
			 */
			$this->update($this->result);
		}
	}

	/**
	 * Update data which is returned by frontend.
	 */
	public function update($data){
		$this->result = $data;
		// First entry should be title.
		array_unshift($data,array('id', 'name', 'state', 'zip', 'amount', 'qty', 'item'));

		try {
			/**
			 * Open File.
			 */
			
			if ( !file_exists($this->file_name) ) {
				/**
				 * File Not Found.
				 */
				throw new Exception('Not found.', 404);
			}
			$file_to_read = fopen($this->file_name, 'w');
			if ( !$file_to_read) {
				/**
				 * File Open Failed.
				 */
				throw new Exception('Permission Denied.', 403);
			} else {
				/**
				 * Return the file handler.
				 */
				foreach($data as $value){
					// insert the each array in to each line.
					fputcsv($file_to_read, $value);
				}
				// Close the file.
				fclose($file_to_read);
			}
		} catch ( Exception $e ) {
			// send error response codes.
			echo http_response_code($e->getCode());
		}

	}

	/**
	 * Send data to frontend as json format.
	 */
	public function read_csv(){
		try {
			/**
			 * Open File.
			 */			
			if ( !file_exists($this->file_name) ) {
				/**
				 * File Not Found.
				 */
				throw new Exception('Not found.', 404);
			}
			$file_to_read = fopen($this->file_name, 'r');
			if ( !$file_to_read) {
				/**
				 * File Open Failed.
				 */
				throw new Exception('Permission Denied.', 403);
			} else {
				$row = 0;
				/**
				 * Iterate the lines of CSV.
				 */
				while(($data = fgetcsv($file_to_read, 100, ',')) !== FALSE){
					if($row == 0){
						$keys = $data;
					}else{
						for($i = 0; $i < count($data); $i++) {
							/**
							 * Insert each line as entry in Result array as Associative array.
							 */
							if (array_key_exists($i,$keys)){
								$this->result[$row][$keys[$i]] = $data[$i];
							}
						}
					}
					$row++;
				}

				/**
				 * Close the file.
				 */
				fclose($file_to_read);

				// Json Encode the array and display it
				echo json_encode($this->result);
			}
		} catch ( Exception $e ) {
			// send error response codes.
			return $this->result = http_response_code($e->getCode());
		}
		
	}
}

/**
 * Create $data object to access API methods.
 */
$data = new Data();

 
	