<?php
/**
 * Set headers to access the origin and content type from Frontend.
 */
header("Access-Control-Allow-Origin: *");
header("Content-Type:application/json");

/**
 * include the Class file to access their members.
 */
include_once "class-data.php";

