<?php
/**
 * Test Case Class
 *
 * @package    Test Case Class
 */

/**
 * import PHPUnit\Framework\TestCase using Composer.
 */
use PHPUnit\Framework\TestCase;

/**
 * include class file to test their members.
 */
require 'class-data.php';

/**
 * DataTest Case Class which extends TestCase generated in PHPUnit\Framework
 */
class DataTest extends TestCase {

    /**
     * testNegativeTestcaseForAssertEmpty.
     */
    public function testNegativeTestcaseForAssertEmpty() {

        /**
         * Get the json data.
         */
        $dataHolder = $data->result;
  
        // Assert function to test whether given.
        // data holder (variable) is empty or not.
        $this->assertEmpty(
            $dataHolder,
            "data holder is not empty"
        );
    }
 
}
